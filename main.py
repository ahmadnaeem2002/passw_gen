#Strong Password Generation
#This app make a strong passowrd 30% Lowercase, 30% Uppercase, 20% Digits, 20% Punctuation
#Version 0.1


#Import Section
import string
import random

#Input Section
list1 = list(string.ascii_lowercase)
list2 = list(string.ascii_uppercase)
list3 = list(string.digits)
list4 = list(string.punctuation)

repeate = True
while repeate == True:
    #Check Characters Number
    number_of_Password  = input("Type the Number of the password Characrers You need: ")
    while True:
        try:
            number_of_Password = int(number_of_Password)
            if number_of_Password > 52:
                print("The Password Must be less than 52 charactars")
                number_of_Password = input("Please Enter another number: ")
            elif number_of_Password < 8 :
                print("The Password Must be at least 8 charactars")
                number_of_Password = input("Please Enter another number: ")
            else:
                break
        except:
            print("You can't type Litters or digits!!!")
            number_of_Password = input("Please Enter The Number Again: ")

    #Shuffling Lists
    random.shuffle(list1)
    random.shuffle(list2)
    random.shuffle(list3)
    random.shuffle(list4)

    part1 = round(number_of_Password * (30/100))
    part2 = round(number_of_Password * (20/100))

    FinalPassword = []
    for i in range(part1):
        FinalPassword.append(list1[i])
        FinalPassword.append(list2[i])
    for i in range(part2):
        FinalPassword.append(list3[i])
        FinalPassword.append(list4[i])

    random.shuffle(FinalPassword)

    FinalPassword = "".join(FinalPassword[0:])
    print("#.......................#.......................#")
    print("Copy the Next Line:",sep="/n")
    print(FinalPassword)
    print("#.......................#.......................#")
    again = input("Are you need Generate again? y/n")
    if again == "n":
        repeate = False
        print("Thanks to use ower application")
    else:
        continue
